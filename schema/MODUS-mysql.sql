CREATE TABLE `Modus_Soil_Test_v1` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `analyte_id` int,
  `extraction_method_id` int,
  `measurement_method_id` int,
  `unit_of_measurment_default_id` int
);

CREATE TABLE `modusSoilv1_altUnitsOfMeasurement` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `modus_soil_test_v1_id` int,
  `unit_of_measurment_id` int
);

CREATE TABLE `Modus_Soil_Test_v2` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `analyte_id` int,
  `extraction_method_id` int,
  `extraction_reagent_formulation_id` int,
  `extraction_ratio_id` int,
  `extraction_basis_id` int,
  `extraction_time_id` int,
  `measurement_method_id` int,
  `aggregation_method` ENUM ('Measured', 'Calculated'),
  `unit_of_measurment_default_id` int,
  `unit_of_measurement_alt_id` int,
  `acceptance` ENUM ('Official', 'Experimental', 'Provisional', 'Proprietary'),
  `status` ENUM ('Active', 'Deprecated'),
  `citation` varchar(255),
  `modus_soil_test_v1_id` int
);

CREATE TABLE `modusSoilv2_organizations` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `organization_id` int,
  `modus_soil_test_v2_id` int
);

CREATE TABLE `Organization` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `Modus_Botanical_Test_v1` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `analyte_id` int,
  `common_name` varchar(255),
  `extraction_method_id` int,
  `measurement_method_id` int,
  `unit_of_expression_id` int
);

CREATE TABLE `modusBotanicalv1_altUnitsOfExpression` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `modus_botanical_test_v1_id` int,
  `unit_of_expression_id` int
);

CREATE TABLE `Modus_Botanical_Test_v2` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `analyte_id` int,
  `common_name` varchar(255),
  `extraction_method_id` int,
  `measurement_method_id` int,
  `unit_of_expression_id` int,
  `extraction_reagent_formulation_id` int,
  `extraction_ratio_id` int,
  `extraction_basis_id` int,
  `unit_of_measurment_default_id` int,
  `unit_of_measurement_alt_id` int,
  `acceptance` ENUM ('Official', 'Experimental', 'Provisional', 'Proprietary'),
  `citation` varchar(255)
);

CREATE TABLE `modusBotanicalv2_organizations` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `organization_id` int,
  `modus_botanical_test_v2_id` int
);

CREATE TABLE `Analytes` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `analyte_name` varchar(255)
);

CREATE TABLE `Extraction_Methods` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `extraction_method_name` varchar(255)
);

CREATE TABLE `Measurement_Methods` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `measurement_method_name` varchar(255)
);

CREATE TABLE `Extraction_Reagent_Formulation` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `extraction_reagent_formulation_name` varchar(255)
);

CREATE TABLE `Extraction_Ratio` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `extraction_ratio_name` varchar(255)
);

CREATE TABLE `Extraction_Basis` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `extraction_basis_name` varchar(255)
);

CREATE TABLE `Extraction_Time` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `extraction_time_name` varchar(255)
);

CREATE TABLE `Units_of_Measurment` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `unit_of_measurement_name` varchar(255),
  `ucum_unit_string` varchar(255)
);

CREATE TABLE `Labs` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `lab_name` varchar(255),
  `testing_service_website` varchar(255),
  `location` varchar(255),
  `contact_email` varchar(255),
  `example_test_result_url` varchar(255)
);

CREATE TABLE `Lab_Certifications` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `certification_id` int,
  `lab_id` int
);

CREATE TABLE `Lab_Packages` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255),
  `lab_id` int,
  `package_link` varchar(255),
  `status` ENUM ('Active', 'Deprecated')
);

CREATE TABLE `Lab_Methods_Modus` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `lab_method_id` int,
  `element_type` ENUM ('soil', 'botanical'),
  `modus_id` varchar(255),
  `modus_soil_test_v1_id` int,
  `modus_soil_test_v2_id` int,
  `modus_botanical_test_v1` int,
  `modus_botanical_test_v2` int
);

CREATE TABLE `Lab_Specified_Methods` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `lab_id` int,
  `element_type` ENUM ('soil', 'botanical'),
  `analyte_id` int,
  `extraction_method_id` int,
  `extraction_regent_formulation_id` int,
  `extraction_ratio_id` int,
  `extraction_basis_id` int,
  `extraction_time_id` int,
  `measurement_method_id` int,
  `aggregation_method` ENUM ('Measured', 'Calculated'),
  `unit_of_measurment_default_id` int,
  `unit_of_measurement_alt_id` int,
  `acceptance` ENUM ('Official', 'Experimental', 'Provisional', 'Proprietary'),
  `status` ENUM ('Active', 'Deprecated'),
  `citation` varchar(255)
);

CREATE TABLE `Certifications` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `certification_name` varchar(255),
  `certification_agency` varchar(255)
);

CREATE TABLE `lab_packages_methods` (
  `id` int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `package_id` int,
  `medhod_id` int
);

ALTER TABLE `Modus_Soil_Test_v1` ADD FOREIGN KEY (`analyte_id`) REFERENCES `Analytes` (`id`);

ALTER TABLE `Modus_Soil_Test_v1` ADD FOREIGN KEY (`extraction_method_id`) REFERENCES `Extraction_Methods` (`id`);

ALTER TABLE `Modus_Soil_Test_v1` ADD FOREIGN KEY (`measurement_method_id`) REFERENCES `Measurement_Methods` (`id`);

ALTER TABLE `Modus_Soil_Test_v1` ADD FOREIGN KEY (`unit_of_measurment_default_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `modusSoilv1_altUnitsOfMeasurement` ADD FOREIGN KEY (`modus_soil_test_v1_id`) REFERENCES `Modus_Soil_Test_v1` (`id`);

ALTER TABLE `modusSoilv1_altUnitsOfMeasurement` ADD FOREIGN KEY (`unit_of_measurment_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`analyte_id`) REFERENCES `Analytes` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`extraction_method_id`) REFERENCES `Extraction_Methods` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`extraction_reagent_formulation_id`) REFERENCES `Extraction_Reagent_Formulation` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`extraction_ratio_id`) REFERENCES `Extraction_Ratio` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`extraction_basis_id`) REFERENCES `Extraction_Basis` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`extraction_time_id`) REFERENCES `Extraction_Time` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`measurement_method_id`) REFERENCES `Measurement_Methods` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`unit_of_measurment_default_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`unit_of_measurement_alt_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Soil_Test_v2` ADD FOREIGN KEY (`modus_soil_test_v1_id`) REFERENCES `Modus_Soil_Test_v1` (`extraction_method_id`);

ALTER TABLE `modusSoilv2_organizations` ADD FOREIGN KEY (`organization_id`) REFERENCES `Organization` (`id`);

ALTER TABLE `modusSoilv2_organizations` ADD FOREIGN KEY (`modus_soil_test_v2_id`) REFERENCES `Modus_Soil_Test_v2` (`id`);

ALTER TABLE `Modus_Botanical_Test_v1` ADD FOREIGN KEY (`analyte_id`) REFERENCES `Analytes` (`id`);

ALTER TABLE `Modus_Botanical_Test_v1` ADD FOREIGN KEY (`extraction_method_id`) REFERENCES `Extraction_Methods` (`id`);

ALTER TABLE `Modus_Botanical_Test_v1` ADD FOREIGN KEY (`measurement_method_id`) REFERENCES `Measurement_Methods` (`id`);

ALTER TABLE `Modus_Botanical_Test_v1` ADD FOREIGN KEY (`unit_of_expression_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `modusBotanicalv1_altUnitsOfExpression` ADD FOREIGN KEY (`modus_botanical_test_v1_id`) REFERENCES `Modus_Botanical_Test_v1` (`id`);

ALTER TABLE `modusBotanicalv1_altUnitsOfExpression` ADD FOREIGN KEY (`unit_of_expression_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`analyte_id`) REFERENCES `Analytes` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`extraction_method_id`) REFERENCES `Extraction_Methods` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`measurement_method_id`) REFERENCES `Measurement_Methods` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`unit_of_expression_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`extraction_reagent_formulation_id`) REFERENCES `Extraction_Reagent_Formulation` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`extraction_ratio_id`) REFERENCES `Extraction_Ratio` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`extraction_basis_id`) REFERENCES `Extraction_Basis` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`unit_of_measurment_default_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Modus_Botanical_Test_v2` ADD FOREIGN KEY (`unit_of_measurement_alt_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `modusBotanicalv2_organizations` ADD FOREIGN KEY (`organization_id`) REFERENCES `Organization` (`id`);

ALTER TABLE `modusBotanicalv2_organizations` ADD FOREIGN KEY (`modus_botanical_test_v2_id`) REFERENCES `Modus_Botanical_Test_v2` (`id`);

ALTER TABLE `Lab_Certifications` ADD FOREIGN KEY (`certification_id`) REFERENCES `Certifications` (`id`);

ALTER TABLE `Lab_Certifications` ADD FOREIGN KEY (`lab_id`) REFERENCES `Labs` (`id`);

ALTER TABLE `Lab_Packages` ADD FOREIGN KEY (`lab_id`) REFERENCES `Labs` (`id`);

ALTER TABLE `Lab_Methods_Modus` ADD FOREIGN KEY (`lab_method_id`) REFERENCES `Lab_Specified_Methods` (`id`);

ALTER TABLE `Lab_Methods_Modus` ADD FOREIGN KEY (`modus_soil_test_v1_id`) REFERENCES `Modus_Soil_Test_v1` (`id`);

ALTER TABLE `Lab_Methods_Modus` ADD FOREIGN KEY (`modus_soil_test_v2_id`) REFERENCES `Modus_Soil_Test_v2` (`id`);

ALTER TABLE `Lab_Methods_Modus` ADD FOREIGN KEY (`modus_botanical_test_v1`) REFERENCES `Modus_Botanical_Test_v1` (`id`);

ALTER TABLE `Lab_Methods_Modus` ADD FOREIGN KEY (`modus_botanical_test_v2`) REFERENCES `Modus_Botanical_Test_v2` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`lab_id`) REFERENCES `Labs` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`analyte_id`) REFERENCES `Analytes` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`extraction_method_id`) REFERENCES `Extraction_Methods` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`extraction_regent_formulation_id`) REFERENCES `Extraction_Reagent_Formulation` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`extraction_ratio_id`) REFERENCES `Extraction_Ratio` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`extraction_basis_id`) REFERENCES `Extraction_Basis` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`extraction_time_id`) REFERENCES `Extraction_Time` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`measurement_method_id`) REFERENCES `Measurement_Methods` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`unit_of_measurment_default_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `Lab_Specified_Methods` ADD FOREIGN KEY (`unit_of_measurement_alt_id`) REFERENCES `Units_of_Measurment` (`id`);

ALTER TABLE `lab_packages_methods` ADD FOREIGN KEY (`package_id`) REFERENCES `Lab_Packages` (`id`);

ALTER TABLE `lab_packages_methods` ADD FOREIGN KEY (`medhod_id`) REFERENCES `Lab_Specified_Methods` (`id`);
