CREATE TABLE [Modus_Soil_Test_v1] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [analyte_id] int,
  [extraction_method_id] int,
  [measurement_method_id] int,
  [unit_of_measurment_default_id] int
)
GO

CREATE TABLE [modusSoilv1_altUnitsOfMeasurement] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [modus_soil_test_v1_id] int,
  [unit_of_measurment_id] int
)
GO

CREATE TABLE [Modus_Soil_Test_v2] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [analyte_id] int,
  [extraction_method_id] int,
  [extraction_reagent_formulation_id] int,
  [extraction_ratio_id] int,
  [extraction_basis_id] int,
  [extraction_time_id] int,
  [measurement_method_id] int,
  [aggregation_method] nvarchar(255) NOT NULL CHECK ([aggregation_method] IN ('Measured', 'Calculated')),
  [unit_of_measurment_default_id] int,
  [unit_of_measurement_alt_id] int,
  [acceptance] nvarchar(255) NOT NULL CHECK ([acceptance] IN ('Official', 'Experimental', 'Provisional', 'Proprietary')),
  [status] nvarchar(255) NOT NULL CHECK ([status] IN ('Active', 'Deprecated')),
  [citation] nvarchar(255),
  [modus_soil_test_v1_id] int
)
GO

CREATE TABLE [modusSoilv2_organizations] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [organization_id] int,
  [modus_soil_test_v2_id] int
)
GO

CREATE TABLE [Organization] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [name] nvarchar(255)
)
GO

CREATE TABLE [Modus_Botanical_Test_v1] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [analyte_id] int,
  [common_name] nvarchar(255),
  [extraction_method_id] int,
  [measurement_method_id] int,
  [unit_of_expression_id] int
)
GO

CREATE TABLE [modusBotanicalv1_altUnitsOfExpression] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [modus_botanical_test_v1_id] int,
  [unit_of_expression_id] int
)
GO

CREATE TABLE [Modus_Botanical_Test_v2] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [analyte_id] int,
  [common_name] nvarchar(255),
  [extraction_method_id] int,
  [measurement_method_id] int,
  [unit_of_expression_id] int,
  [extraction_reagent_formulation_id] int,
  [extraction_ratio_id] int,
  [extraction_basis_id] int,
  [unit_of_measurment_default_id] int,
  [unit_of_measurement_alt_id] int,
  [acceptance] nvarchar(255) NOT NULL CHECK ([acceptance] IN ('Official', 'Experimental', 'Provisional', 'Proprietary')),
  [citation] nvarchar(255)
)
GO

CREATE TABLE [modusBotanicalv2_organizations] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [organization_id] int,
  [modus_botanical_test_v2_id] int
)
GO

CREATE TABLE [Analytes] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [analyte_name] nvarchar(255)
)
GO

CREATE TABLE [Extraction_Methods] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [extraction_method_name] nvarchar(255)
)
GO

CREATE TABLE [Measurement_Methods] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [measurement_method_name] nvarchar(255)
)
GO

CREATE TABLE [Extraction_Reagent_Formulation] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [extraction_reagent_formulation_name] nvarchar(255)
)
GO

CREATE TABLE [Extraction_Ratio] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [extraction_ratio_name] nvarchar(255)
)
GO

CREATE TABLE [Extraction_Basis] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [extraction_basis_name] nvarchar(255)
)
GO

CREATE TABLE [Extraction_Time] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [extraction_time_name] nvarchar(255)
)
GO

CREATE TABLE [Units_of_Measurment] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [unit_of_measurement_name] nvarchar(255),
  [ucum_unit_string] nvarchar(255)
)
GO

CREATE TABLE [Labs] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [lab_name] nvarchar(255),
  [testing_service_website] nvarchar(255),
  [location] nvarchar(255),
  [contact_email] nvarchar(255),
  [example_test_result_url] nvarchar(255)
)
GO

CREATE TABLE [Lab_Certifications] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [certification_id] int,
  [lab_id] int
)
GO

CREATE TABLE [Lab_Packages] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [package_name] nvarchar(255),
  [lab_id] int,
  [package_link] nvarchar(255),
  [status] nvarchar(255) NOT NULL CHECK ([status] IN ('Active', 'Deprecated'))
)
GO

CREATE TABLE [Lab_Methods_Modus] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [lab_method_id] int,
  [element_type] nvarchar(255) NOT NULL CHECK ([element_type] IN ('soil', 'botanical')),
  [modus_id] nvarchar(255),
  [modus_soil_test_v1_id] int,
  [modus_soil_test_v2_id] int,
  [modus_botanical_test_v1] int,
  [modus_botanical_test_v2] int
)
GO

CREATE TABLE [Lab_Specified_Methods] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [lab_id] int,
  [element_type] nvarchar(255) NOT NULL CHECK ([element_type] IN ('soil', 'botanical')),
  [analyte_id] int,
  [extraction_method_id] int,
  [extraction_regent_formulation_id] int,
  [extraction_ratio_id] int,
  [extraction_basis_id] int,
  [extraction_time_id] int,
  [measurement_method_id] int,
  [aggregation_method] nvarchar(255) NOT NULL CHECK ([aggregation_method] IN ('Measured', 'Calculated')),
  [unit_of_measurment_default_id] int,
  [unit_of_measurement_alt_id] int,
  [acceptance] nvarchar(255) NOT NULL CHECK ([acceptance] IN ('Official', 'Experimental', 'Provisional', 'Proprietary')),
  [status] nvarchar(255) NOT NULL CHECK ([status] IN ('Active', 'Deprecated')),
  [citation] nvarchar(255)
)
GO

CREATE TABLE [Certifications] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [certification_name] nvarchar(255),
  [certification_agency] nvarchar(255)
)
GO

CREATE TABLE [lab_packages_methods] (
  [id] int UNIQUE PRIMARY KEY NOT NULL IDENTITY(1, 1),
  [package_id] int,
  [medhod_id] int
)
GO

ALTER TABLE [Modus_Soil_Test_v1] ADD FOREIGN KEY ([analyte_id]) REFERENCES [Analytes] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v1] ADD FOREIGN KEY ([extraction_method_id]) REFERENCES [Extraction_Methods] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v1] ADD FOREIGN KEY ([measurement_method_id]) REFERENCES [Measurement_Methods] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v1] ADD FOREIGN KEY ([unit_of_measurment_default_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [modusSoilv1_altUnitsOfMeasurement] ADD FOREIGN KEY ([modus_soil_test_v1_id]) REFERENCES [Modus_Soil_Test_v1] ([id])
GO

ALTER TABLE [modusSoilv1_altUnitsOfMeasurement] ADD FOREIGN KEY ([unit_of_measurment_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([analyte_id]) REFERENCES [Analytes] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([extraction_method_id]) REFERENCES [Extraction_Methods] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([extraction_reagent_formulation_id]) REFERENCES [Extraction_Reagent_Formulation] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([extraction_ratio_id]) REFERENCES [Extraction_Ratio] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([extraction_basis_id]) REFERENCES [Extraction_Basis] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([extraction_time_id]) REFERENCES [Extraction_Time] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([measurement_method_id]) REFERENCES [Measurement_Methods] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([unit_of_measurment_default_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([unit_of_measurement_alt_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Soil_Test_v2] ADD FOREIGN KEY ([modus_soil_test_v1_id]) REFERENCES [Modus_Soil_Test_v1] ([extraction_method_id])
GO

ALTER TABLE [modusSoilv2_organizations] ADD FOREIGN KEY ([organization_id]) REFERENCES [Organization] ([id])
GO

ALTER TABLE [modusSoilv2_organizations] ADD FOREIGN KEY ([modus_soil_test_v2_id]) REFERENCES [Modus_Soil_Test_v2] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v1] ADD FOREIGN KEY ([analyte_id]) REFERENCES [Analytes] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v1] ADD FOREIGN KEY ([extraction_method_id]) REFERENCES [Extraction_Methods] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v1] ADD FOREIGN KEY ([measurement_method_id]) REFERENCES [Measurement_Methods] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v1] ADD FOREIGN KEY ([unit_of_expression_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [modusBotanicalv1_altUnitsOfExpression] ADD FOREIGN KEY ([modus_botanical_test_v1_id]) REFERENCES [Modus_Botanical_Test_v1] ([id])
GO

ALTER TABLE [modusBotanicalv1_altUnitsOfExpression] ADD FOREIGN KEY ([unit_of_expression_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([analyte_id]) REFERENCES [Analytes] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([extraction_method_id]) REFERENCES [Extraction_Methods] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([measurement_method_id]) REFERENCES [Measurement_Methods] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([unit_of_expression_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([extraction_reagent_formulation_id]) REFERENCES [Extraction_Reagent_Formulation] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([extraction_ratio_id]) REFERENCES [Extraction_Ratio] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([extraction_basis_id]) REFERENCES [Extraction_Basis] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([unit_of_measurment_default_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Modus_Botanical_Test_v2] ADD FOREIGN KEY ([unit_of_measurement_alt_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [modusBotanicalv2_organizations] ADD FOREIGN KEY ([organization_id]) REFERENCES [Organization] ([id])
GO

ALTER TABLE [modusBotanicalv2_organizations] ADD FOREIGN KEY ([modus_botanical_test_v2_id]) REFERENCES [Modus_Botanical_Test_v2] ([id])
GO

ALTER TABLE [Lab_Certifications] ADD FOREIGN KEY ([certification_id]) REFERENCES [Certifications] ([id])
GO

ALTER TABLE [Lab_Certifications] ADD FOREIGN KEY ([lab_id]) REFERENCES [Labs] ([id])
GO

ALTER TABLE [Lab_Packages] ADD FOREIGN KEY ([lab_id]) REFERENCES [Labs] ([id])
GO

ALTER TABLE [Lab_Methods_Modus] ADD FOREIGN KEY ([lab_method_id]) REFERENCES [Lab_Specified_Methods] ([id])
GO

ALTER TABLE [Lab_Methods_Modus] ADD FOREIGN KEY ([modus_soil_test_v1_id]) REFERENCES [Modus_Soil_Test_v1] ([id])
GO

ALTER TABLE [Lab_Methods_Modus] ADD FOREIGN KEY ([modus_soil_test_v2_id]) REFERENCES [Modus_Soil_Test_v2] ([id])
GO

ALTER TABLE [Lab_Methods_Modus] ADD FOREIGN KEY ([modus_botanical_test_v1]) REFERENCES [Modus_Botanical_Test_v1] ([id])
GO

ALTER TABLE [Lab_Methods_Modus] ADD FOREIGN KEY ([modus_botanical_test_v2]) REFERENCES [Modus_Botanical_Test_v2] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([lab_id]) REFERENCES [Labs] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([analyte_id]) REFERENCES [Analytes] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([extraction_method_id]) REFERENCES [Extraction_Methods] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([extraction_regent_formulation_id]) REFERENCES [Extraction_Reagent_Formulation] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([extraction_ratio_id]) REFERENCES [Extraction_Ratio] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([extraction_basis_id]) REFERENCES [Extraction_Basis] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([extraction_time_id]) REFERENCES [Extraction_Time] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([measurement_method_id]) REFERENCES [Measurement_Methods] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([unit_of_measurment_default_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [Lab_Specified_Methods] ADD FOREIGN KEY ([unit_of_measurement_alt_id]) REFERENCES [Units_of_Measurment] ([id])
GO

ALTER TABLE [lab_packages_methods] ADD FOREIGN KEY ([package_id]) REFERENCES [Lab_Packages] ([id])
GO

ALTER TABLE [lab_packages_methods] ADD FOREIGN KEY ([medhod_id]) REFERENCES [Lab_Specified_Methods] ([id])
GO
