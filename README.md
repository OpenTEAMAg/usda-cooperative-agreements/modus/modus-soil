# Scope and agreements
This repository is the artifact archive of OpenTEAM's effort to enabling comparison and aggregation of soil test results across different labs and USDA programs, under the PODS Cooperative Agreement between USDA NRCS and OpenTEAM. See the [Summary Report](Summary_Report.MD) for more information about the project.

# Modus Soil Nomenclature
The official Modus Soil Nomenclature is maintained by [Ag Gateway](https://aggateway.atlassian.net/wiki/spaces/AG/overview?mode=global_). OpenTEAM maintains this secondary nomenclature built on the formal nomenclature to include needs of our working groups, while under review for incorporation by Ag Gateway.

To request new soil test id's from Ag Gateway, use this link: [https://aggateway.atlassian.net/wiki/spaces/MOD/pages/3465249484/Modus+Addition+Change+Request+Form](https://aggateway.atlassian.net/wiki/spaces/MOD/pages/3465249484/Modus+Addition+Change+Request+Form)

## Terms of Use
In accordance to [Ag Gateway's Data Resource License](https://github.com/AgGateway/Modus/blob/main/LICENSE.md), we are authorized to create derivations of and distribute the their Modus data resource. All users of the Modus nomenclature component of this repository are considered Secondary Licensees, and must recieve permission from Ag Gateway to create further derivations and distributions of the nomenclature data.

The inventory of methods conducted by labs in this library are from publically available resources (e.g., a lab's website) or contributed directly by a lab representative for public consumption. This work may used and distributed in accordance to [CC BY-ND](https://creativecommons.org/licenses/by-nd/4.0/). 

# Contents of this Repository

- /archive
  - contains an archive of the lab method library in excel.
- /data
  - contains csv and json data that can be imported into [this Airtable template](https://www.airtable.com/universe/exp3E2fW6V5zStN3L/modus-soil-lab-method-library) or a sql database.
- /schema
  - contains sql exports of this [ERD](img/MODUS-erd.png) from dbdiagram.io.
- /img
  - contain logos and Modus Lab Method Library ERD image

The csv and json exports in this repository represent flattened Modus Nomenclature and Lab Methods dataset, meaning the ids of linked parameters are not preserved, but instead the value is listed.

The excel file is an archival product and final deliverable of the Modus Soil Lab Method Library to the USDA NRCS.

## Data Dictionary
Definition of our headers, and mapping of our headers to Modus nomenclature coming soon.

# OpenTEAM Modus Nomenclature + Lab Methods Airtable Base
We are currently maintaining a nomenclature and lab methods database in Airtable because it affords rapid prototyping and data generation, though it is unlikely to be where this dataset lives in the long-term.

Read view of the Airtable database maintained by OpenTEAM: [https://airtable.com/appcE8fUEjo5h7cMt/shrukjlgeq0gSQTaJ](https://airtable.com/appcE8fUEjo5h7cMt/shrukjlgeq0gSQTaJ)

We have have incorporated the publicly release v1 Soil Nomenclature, v1 Botanical Nomenclature, as well as the pre-release v2 Soil Nomenclature. In each case, we work on creating a machine readable version of the formal dataset.

## Airtable Field and Table name conventions
In this [airtable base](https://www.airtable.com/universe/exp3E2fW6V5zStN3L/modus-soil-lab-method-library) we follow this set of field name conventions:
- Table names are Capitalized with underscores in place of spaces. e.g., `Measurement_Method`
- Fields containing any values that is not a linked record, look up value from a different record, or a formula should be lowercase with underscores instead of spaces e.g., `lab_method_name`
- Fields containing linked records in another table should use the table name, with underscores and capitilizations. e.g., `Modus_Soil_Test-v1`
- Fields containing look-up values from another table should lowercase the table name, followed by an underscore and the name of that field in its native table. e.g., `modus_soil_test-v1_id`
- When versioning is involved, for example when a table represents a new nomenclature version, add a version suffix like `"-v1"` to the end of all tables names and fields that need to be distinguished for a specific nomenclature. e.g., `Modus_Soil_Test-v1`
- Fields containing formulas should begin with an `"f-"` prefix then follow the above conventions depending on what data the formula is displaying, with an optional `"-[operation]"` suffix
  - for example if its choosing between one or more linked Measurement_Methods records in context of v1 nomenclature, `f-Measurement_Method-v1`

## Mapping Modus nomenclature to Lab Methods
OpenTEAM is working on building out a library of Lab Methods and their corresponding Modus IDs, starting with [this list of labs]](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/proj-mgmt/uploads/ee665ba6e5f8488fc76ed2b29be7702c/SoilTestingLabs__1_.docx) identified for this project and utilized by other partner organizations.

This process first involves an analysis of information on a lab's public facing website to pre-populate lab test data and probably Modus Id assignments. Then a member of our team reaches out to the lab to review and revies the preliminary dataset, determine which of the lab's tests are not in Modus, and submit requests for new ids.
