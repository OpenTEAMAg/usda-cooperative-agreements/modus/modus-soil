## Standard Operating Procedures for Lab Method Library
A standard operating procedure (SOP) is a set of instructions explaining a process in an organizational context, explaining how, when, and where a task and process should be carried out, and who conducts the activities. Creating SOPs offer an opportunity for an organization to formalize processes and address idiosyncrasies. We create SOPs at the individual process level. Most of these processes can be completed by a single person, however some processes require collaboration with other stakeholders and may have additional complexities. To support easier, and quicker understanding of processes, we supplement our SOPs with process mapping to synthesize and visualize operating procedures.  

### SOP: Lab Method Data Collection
![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/figure37-page116-appendix9-cropped.png)
*Process map linking the three lab method data collection activities.*

### Activity: Lab Engagement in SurveyStack
Purpose  
The Modus - Lab Method Entry SurveyStack survey was created and designed to allow lab staff to submit their methods to match the format of Modus IDs. The survey is exportable as a CSV and can be imported to Airtable, where a data steward can then complete the lab’s Modus ID associations in the library.  

Context Specification  
The Modus data steward or product owner will first get in touch with a lab contact, ideally a member of the technical staff. They explain Modus and ask them to input their lab’s methods into the Modus - Lab Method Entry survey.  This eliminates transcription errors and minimizes back and forth between the data steward and labs.  

Procedure  

| Start: | Data steward or product owner will assemble a list of labs to target, i.e. USDA’s list of CEMA recommended labs. For each of the labs, follow these steps: |
| ------ | ------ |
|    Step 1:    |     Contact lab personnel via email.   |
|    Step 2:    |    Once in contact with the lab, organize a video chat with the appropriate lab contact to onboard and acquaint them with the method input survey. Highlight the project and what their task is and how we will complete the method association for them. Ensure they are aware of a timeline and that there may be follow up questions about their methods.     |
| Step 3: | They will complete the survey. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.1-page118-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.2-page118-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.3-page119-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.4-page119-appendix9-cropped.png) Note: The lab will use the slider bar to complete all the fields in Q4, below. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.5-page119-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step1.3.6-page120-appendix9-cropped.png) |
| Step 4: | Check the survey results page for new submissions at a daily or weekly cadence. If they have not submitted a completed survey by the timeline, follow up via email with the labs bi-weekly to see if they have encountered any obstacles or questions.  |
| Step 5: | Review submitted surveys for completion. | 
| Stop 1 | Download completed surveys as a CSV and upload it to Airtable. See the section below for SOP: Data Transfer from Lab Survey to Lab Method Library. | 
| Stop 2 | Send confirmation of submission and thank you email to the lab. |


### Activity: Data Transfer from Lab Survey to Lab Method Library

Purpose  
The purpose of moving data from survey stack to the lab method library is to update the inventory of methods conducted by labs in the Lab Method Library.  

Context Specification  
This process comes after Lab Method Data Collection. It requires an understanding of the lab method library data structure, and an ability to transform non-uniform data into the library data standard. This should be conducted by the data steward or product owner of the Lab Method Library.  

Procedure  
| Start | Pull CSV export from SurveStack Lab Method Entry |
| ------ | ------ | 
| Step 1: | Make sure the SurveyStack import table in Airtable is empty. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.1-page121-appendix9-cropped.png) |
| Step 2: | Import CSV and exclude first row from the data. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.2.1-page122-appendix9-original_final.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.2.2-page122-appendix9-cropped.png) |
| Step 3: | Copy/paste the lab name to the empty rows. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.3.1-page123-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.3.2-page123-appendix9-cropped.png) |
| Step 4: | Delete first row for each lab (should not contain data in the fields starting with  “data.matrix.table_of_methods”). ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.4-page123-appendix9-cropped.png) |
| Step 5: | Review Transformation Automation, fix any mistakes, add new library terms (use discretion). ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.5.1-page123-appendix9-cropped.png) e.g., unitless ratio is not in the UOM library, but ratio is. Manually update the translate field to ratio. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.5.2-page124-appendix9-cropped.png) |
| Step 6: | Check the import box to add method to the library. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.6-page124-appendix9-cropped.png) |
| Step 7: | Review new methods in the library. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.7-page125-appendix9-cropped.png) | 
| Stop 1 | Map to Modus V2 nomenclature. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.8-page125-appendix9-cropped.png) ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step2.9-page126-appendix9-cropped.png) |
| Stop 2 | If the lab method is not represented in the Modus v2 nomenclature, follow the SOP: Report Lab Method to AgGateway. |

### Activity: Reporting Lab Method to AgGateway

Purpose  
To add a new method or make a change to an existing method in the Modus V2 library.   

Context Specification  
This step is used if there are any methods that do not exist in the Modus Library. After a lab has submitted and mapped their methods to Modus IDs, the data steward or product owner will transcribe the unmatched method from what was submitted by the lab into a form provided by AgGateway. AgGateway determines if a method needs to be changed or if a new method should be created.   

Procedure  
| Start | Ensure you have the Modus Lab Methods Library Airtable base open.  |
| ------ | ------ | 
| Step 1: | Navigate to the Lab_Method tab in airtable and find the lab method that has no exact Modus ID match. |
| Step 2: | Transcribe the method information provided by the lab to AgGateway’s form. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step3.2-page127-appendix9-cropped.png) |
| Step 3: | Select “Send me a copy of my responses” and submit the form. |
| Stop | AgGateway may follow up if they have questions or believe the method already exists in the library. If this occurs, the data steward will coordinate with the lab and AgGateway to answer any questions. Once the method is identified or added, the data steward or product owner will use airtable to associate the Method ID with the applicable lab. ![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/step3.stop-page127-appendix9-cropped.png) |


### Data Journey: Collecting CEMA 216 soil test data using the Modus Standard, Lab Method Library, & Tool Set
This is a conceptual SOP. Not all parts of this SOP exist, but are instead representations of how the Modus Lab Method Library can be incorporated into the PODS Soil health pipeline.   

Purpose  
To capture Soil Health Test data from many labs and enter them into PODS, preserving or defining all lab method meta data to ensure downstream data comparability.  

Context Specification  
This conceptual activity reflects the existing soil health test data journey as experienced in CEMA 216 with modifications that include the Modus Standard, Modus Lab Method Library, and Modus Toolset.  

Data Journey  
![](https://gitlab.com/OpenTEAMAg/usda-cooperative-agreements/modus/modus-soil/-/raw/master/img/data-journey-page130-appendix9-cropped.png)
*Map of the existing soil health test data journey for CEMA 216 highlighting data quality and pain points. The below table demonstrates where the Modus Standard, Lab Method Library, and Toolset can be incorporated to address these painpoints.*

| Start | Farmer Enrolls in CEMA 216. |
| ------ | ------ | 
| Step 1: | Qualified Individual Collects Sample. |
| | Refer to CEMA sampling protocol. |
| | Collect soil samples. |
| Step 2: | Send to a qualifying lab. |
| | Reference CEMA standard for Qualifying Test Methods with assigned MODUS IDs. |
| | Identify Qualifying lab that conducts qualifying tests in Modus Lab Method Library. |
| Step 3: | Farmer receives results from the lab. |
| | Lab has provided results in the Modus schema format (OR) Lab has provided results that associate method with modus ID, (OR) QI uses Modus Toolset to turn lab csv into Modus Standard. |
| Step 4: | Farmer or QI sends management history, sampling map, and lab results to NRCS Soil Health Division. |
| Step 5: | IF Soil health Division receives data that is not associated with MODUS via the above steps, they can use the Lab Method Library to identify which methods are used and use the Modus Toolset to convert the CSV into a modus standard. |
| Stop | Data is in comparable format and is compatible with downstream models working with the Modus Standard. |